<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

define('DB_NAME', 'admin_chillidee');

/** MySQL database username */
define('DB_USER', 'admin_chillidee');

/** MySQL database password */
define('DB_PASSWORD', 'oT4xv2iQHw');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9LIklQU6<4F(I<zYZ z5Nd(7e{*AupdYAF_heaU)A|pS>)X.-=-=A8SVG8Qi[%G,');
define('SECURE_AUTH_KEY',  '2Qlx~i:IY|]~!62(!!1)@6(93XX:F#0*}|; = BQ,<U!)N]1_nJ9;o9-A#N2)B^=');
define('LOGGED_IN_KEY',    '@9W?73`3y{W6x^%%WSdYL[x W&r nd9F!o |Go53C5;2$1#]V29inskD7lEsoexT');
define('NONCE_KEY',        '`+/_/}!*$BAuS.AI}J{tFycY1i5e$ @T42SsK|&%V*GAd]8kj-_+V7_Mi8fKS=@A');
define('AUTH_SALT',        'Ao^ Jff-csz0UoAW3.r,rQCD{%+H;9g%wHjC@[or@?>743B;)(Pf])6Uul#@{i m');
define('SECURE_AUTH_SALT', '@(5}u%DfNw5jwC]I*4ovJOdi`dF~MGRVjZ<m@F^1hiNi$OM4?g&*Yo<3Dx=v`OX#');
define('LOGGED_IN_SALT',   '{6BIKLha]ZY4M#cQ9};0~6aZLvBMV^0{3s0my2^V]kXnbhel;/Wf)A]mS14dk2ck');
define('NONCE_SALT',       'DbNf$]Q!2=0jBpUy5De+u>=V7^!_g[hb9~`1b!{IG#T#:%vKJ|CDl3K_~Z`}DX|b');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'chilli_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** Wordpress version auto-update */
define( 'WP_AUTO_UPDATE_CORE', false );